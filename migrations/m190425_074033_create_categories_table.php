<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%categories}}`.
 */
class m190425_074033_create_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Наименование'),
        ]);

        $this->insert('categories',array(
            'id' => 1,
            'name'=>'Dell',
        ));

        $this->insert('categories',array(
            'id' => 2,
            'name'=>'Asus',
        ));

        $this->insert('categories',array(
            'id' => 3,
            'name'=>'HP',
        ));

        $this->insert('categories',array(
            'id' => 4,
            'name'=>'Acer',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%categories}}');
    }
}
