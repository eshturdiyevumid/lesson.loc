<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%categories}}`
 */
class m190425_074227_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'categoryId' => $this->integer()->notNull()->comment('Категория'),
            'price' => $this->float()->comment('Цена'),
            'hidden' => $this->boolean()->comment('Hidden'),
        ]);

        // creates index for column `categoryId`
        $this->createIndex(
            '{{%idx-products-categoryId}}',
            '{{%products}}',
            'categoryId'
        );

        // add foreign key for table `{{%categories}}`
        $this->addForeignKey(
            '{{%fk-products-categoryId}}',
            '{{%products}}',
            'categoryId',
            '{{%categories}}',
            'id',
            'CASCADE'
        );

        $this->insert('products',array(
            'id' => 1,
            'categoryId' => 1,
            'price'=>100,
            'hidden' => 1,
        ));

        $this->insert('products',array(
            'id' => 2,
            'categoryId' => 2,
            'price'=>200,
            'hidden' => 1,
        ));

        $this->insert('products',array(
            'id' => 3,
            'categoryId' => 1,
            'price'=>300,
            'hidden' => 0,
        ));

        $this->insert('products',array(
            'id' => 4,
            'categoryId' => 3,
            'price' => 400,
            'hidden' => 0,
        ));

        $this->insert('products',array(
            'id' => 5,
            'categoryId' => 1,
            'price'=>500,
            'hidden' => 0,
        ));

        $this->insert('products',array(
            'id' => 6,
            'categoryId' => 3,
            'price'=>600,
            'hidden' => 1,
        ));

        $this->insert('products',array(
            'id' => 7,
            'categoryId' => 1,
            'price'=>700,
            'hidden' => 1,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%categories}}`
        $this->dropForeignKey(
            '{{%fk-products-categoryId}}',
            '{{%products}}'
        );

        // drops index for column `categoryId`
        $this->dropIndex(
            '{{%idx-products-categoryId}}',
            '{{%products}}'
        );

        $this->dropTable('{{%products}}');
    }
}
