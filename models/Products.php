<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $categoryId Категория
 * @property double $price Цена
 * @property int $hidden Hidden
 *
 * @property Categories $category
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoryId'], 'required'],
            [['categoryId', 'hidden'], 'integer'],
            [['price'], 'number'],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['categoryId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoryId' => 'Категория',
            'price' => 'Цена',
            'hidden' => 'Hidden',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'categoryId']);
    }

    public function HiddenText()
    {
        if($this->hidden === 1) return 'Да';
        else if($this->hidden === 0) return 'Нет';
            else return '<i>(не задано)</i>';
    }

    public function HiddenTypes()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }

    public function getCategoriesList()
    {
        $categories = Categories::find()->all();
        return ArrayHelper::map($categories,'id', 'name');
    }
}
