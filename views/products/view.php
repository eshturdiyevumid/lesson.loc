<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
?>
<div class="products-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
            	'attribute' => 'categoryId',
            	'value' => $model->category->name,
            ],
            'price',
            [
            	'attribute' => 'hidden',
            	'value' => $model->HiddenText(),
            ],
        ],
    ]) ?>

</div>
